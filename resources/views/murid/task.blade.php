@extends('layouts.master_siswa')
@section('title', 'Hasil Pencarian')
@section('content')
<div class="container py-5" id="task_page">
  <div class="row justify-content-between m-0">
    @if(count($orders) > 0)
    <section class="col-12">
      <h1 class="h2 font-weight-bold mt-0 mb-4">Daftar Pesananku</h1>
      @foreach($orders as $order)
        <a href="{{route('murid.detail', $order->id) }}" class="no-underline card-task">
          <article class="col-12 card p-0 bg-white">
            <div class="detail_pesanan p-3 position-relative">
              <h2>Pelajaran {{ $order->pelajaran }}</h2>
              <p class="mt-3 mb-0 text-secondary text-left">Materi {{ $order->kategori_pelajaran }}</p>
              <span class="my-1 font-weight-bold" style="color: #16A658;">Les {{ $order->type }}</span>
            </div>
            <div class="detail_tambahan border-top d-flex justify-content-between p-3">
              <span>Status : {{ $order->status }}</span>
              <span class="text-dark">Pesanan Pada Tanggal <time class="font-weight-bold">{{ $order->created_at->format('d F Y') }}</time></span>
              <span class="text-info">
                Harga : <var class="font-weight-bold" style="color: #DE891A;">{{ $order->lesco }} Lesco</var>
              </span>
            </div>
          </article>
        </a>
      @endforeach
    </section>
    <div class="row m-0">
      <div class="col">
        {{ $orders->links('vendor.pagination.bootstrap-4') }}
      </div>
    </div>
    @else
    <div class="alert alert-warning w-100 text-center font-weight-bold" role="alert">
      Kamu Belum Pernah Memesan Guru
    </div>
    @endif
  </div>
</div>
@endsection
@section('footer_all')
  @include('partials.footer')
@endsection
