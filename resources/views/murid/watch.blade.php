@extends('layouts.master_siswa')

@section('title', 'Watching Your Video')

@section('content')

  <video width="320" height="400" controls>
    <source src="{{ asset('uploaded_video/'.$myvideos) }}" type="video/mp4">
  </video>

@endsection

@section('footer_all')

@include('partials.footer')

@endsection
