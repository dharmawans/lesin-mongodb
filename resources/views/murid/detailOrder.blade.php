@extends('layouts.master_siswa')
@section('title', 'Detail Order')
@section('content')
<div class="container py-0" id="order_page">
  <div class="row">
    <div class="col-12 col-md-4 order-md-2 py-md-5">
      <figure class="p-3 p-md-0 m-0 text-center position-relative" style="top: 10%;transform: translateY(-50%)">
        <img src="{{ asset('photoguru/'.$orders->photo_murid) }}" height="90" alt="" class="">
        <figcaption class="pt-2 text-center mt-0">
          <h2 class="m-0">{{ $orders->teachername }}</h2>
          <span class="text-success font-weight-bold">Status : {{ $orders->status }}</span>
          @if ($orders->type == 'Offline')
          <address class="font-weight-bold m-0 text-primary">
            Alamat Ketemuan : {{ $orders->alamat }}
          </address>
          @endif
        </figcaption>
      </figure>
    </div>
    <div class="col-12 col-md-8 py-5 order-md-1">
      <h1 class="col-12 px-0 pb-md-4 h3 text-dark font-weight-bold">Detail Pemesanan {{ $orders->created_at->format('d F Y') }}</h1>
        <div class="row">
          <div class="col">
            <p class="text-secondary">Mata Pelajaran<br><span class="text-success font-weight-bold text-capitalize">{{ $orders->pelajaran }}</span></p>
          </div>
          <div class="col">
            <p class="text-secondary">Materi Pelajaran<br><span class="text-success font-weight-bold text-capitalize">{{ $orders->kategori_pelajaran }}</span></p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <p class="text-secondary">Jenjang Pendidikan<br><span class="text-success font-weight-bold text-uppercase">{{ $orders->jenjang }}</span></p>
          </div>
          <div class="col">
            <p class="text-secondary">Tipe Pesanan Guru<br><span class="text-success font-weight-bold text-capitalize">Les {{ $orders->type }}</span></p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <p class="text-secondary">Durasi<br><span class="text-success font-weight-bold">{{ $orders->durasi }} Jam</span></p>
          </div>
          <div class="col">
            <p class="text-secondary">Tarif Lesco<br><span class="text-success font-weight-bold">{{ $orders->lesco }} Coin</span></p>
          </div>
        </div>
        @if ($orders->type == 'Offline')
        <div class="row">
          <div class="col">
            <label class="text-secondary mb-1">Alamat Ketemuan</label>
            <address class="font-weight-bold text-success">{{ $orders->alamat }}</address>
          </div>
        </div>
        @endif
        <div class="row mb-3" id="deskripsi_pesanan">
          <div class="col">
            <label for="deskripsi" class="text-secondary">Deskripsi Pesanan</label>
            <p class="text-success font-weight-bold text-capitalize">{{ $orders->deskripsi }}</p>
          </div>
        </div>
        @if ($orders->status === "Waiting")
        <div class="row mx-0">
          <a href="{{ route('murid.hapusOrderan', $orders->id) }}" class="btn btn-danger">Hapus Orderan Ini</a>
        </div>
        @elseif($orders->status === "Process")
        <form action="{{ route('murid.createOfflineFinished') }}" method="post">
          @csrf
          <input type="hidden" class="form-control" value="{{ $orders->id }}" name="idOrder">
          <button type="submit" name="button" class="btn btn-danger">Sudahi Belajar</button>
        </form>
        @endif
    </div>
  </div>
  @endsection
  @section('footer_all')
    @include('partials.footer')
  @endsection
