<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Sertifikasi</title>
	<style media="screen">
		* {
			box-sizing: border-box;
			padding: 0;
			margin: 0;
			list-style: none;
			outline: none;
		}
		body {
			padding: 20px 0;
		}
		h1:first-of-type {
			text-align: center;
		}
		ul li {
			color: #159932;
			font-weight: bold;
		}
		footer {
			background-color: #0C414F;
			color: white;
			width: 100%;
			padding: 20px 0;
		}
	</style>
</head>
<body>
	<h1>Sertifikasi Penyelesaian</h1>
	<h2>
		Siswa Yang Bernama <br> <b>@foreach($users as $user){{ $user->name }}@endforeach</b>
	</h2>
	<h3>sudah menyelesaikan</h3>

	@foreach($videos as $video)
	<ul>
		<li>{{ $video->judul }}</li>
	</ul>
	<h3>dari</h3>
	<h2>{{ $video->guru }}</h2>
	@endforeach
	<footer>Sertifikat by Ngeles</footer>
</body>
</html>
