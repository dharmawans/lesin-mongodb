<div id="footer_legal" class="py-3">
  <div class="container">
    <div class="row mx-0">
      <small style="font-size: 17px;">Copyright &copy; Ngeles {{ date('Y') }} | All Rights Reserved</small>
    </div>
  </div>
</div>
