@extends('layouts.master_guru')
@section('title', 'Detail Order')
@section('content')
  <div class="container py-0" id="order_page">
    <div class="row">
      <div class="col-12 col-md-4 order-md-2 py-md-5">
        <figure class="p-3 p-md-0 m-0 text-center position-relative" style="top: 10%;transform: translateY(-50%)">
          <img src="{{ asset('photoguru/' . $orderan->orderOwner->profileMurid->photo) }}" height="90">
          <figcaption class="pt-2 text-center mt-0">
            <h2 class="m-0">{{ $orderan->teachername }}</h2>
            <p class="text-success font-weight-bold m-0">Status : {{ $orderan->status }}</p>
            <p class="text-info m-0 font-weight-bold">
              Nomor Murid :
              <a  class="text-info" href="tel:{{ $orderan->orderOwner->profileMurid->phone }}">
                {{ $orderan->orderOwner->profileMurid->phone }}
              </a>
            </p>
          </figcaption>
        </figure>
      </div>
      <div class="col-12 col-md-8 py-5 order-md-1">
        <h1 class="col-12 px-0 pb-md-4 h3 text-dark font-weight-bold">Detail Pemesanan {{ $orderan->created_at->format('d F Y') }}</h1>
        <div class="row">
          <div class="col">
            <p class="text-secondary">
              Mata Pelajaran <br>
              <span class="text-success font-weight-bold text-capitalize">{{ $orderan->pelajaran }}</span>
            </p>
          </div>
          <div class="col">
            <p class="text-secondary">
              Materi Pelajaran <br>
              <span class="text-success font-weight-bold text-capitalize">{{ $orderan->kategori_pelajaran }}</span>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <p class="text-secondary">
              Jenjang Pendidikan <br>
              <span class="text-success font-weight-bold text-uppercase">{{ $orderan->orderOwner->profileMurid->jenjang }}</span>
            </p>
          </div>
          <div class="col">
            <p class="text-secondary">
              Tipe Pesanan Guru <br>
              <span class="text-success font-weight-bold text-capitalize">Les {{ $orderan->type }}</span>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <p class="text-secondary">Durasi<br><span class="text-success font-weight-bold">{{ $orderan->durasi }} Jam</span></p>
          </div>
          <div class="col">
            <p class="text-secondary">Tarif Lesco<br><span class="text-success font-weight-bold">{{ $orderan->lesco }} Coin</span></p>
          </div>
        </div>
        @if ($orderan->type == 'Offline')
        <div class="row">
          <div class="col">
            <label class="text-secondary mb-1">Alamat Ketemuan</label>
            <address class="font-weight-bold text-success">{{ $orderan->alamat_ketemu }}</address>
          </div>
        </div>
        @endif
        <div class="row mb-3" id="deskripsi_pesanan">
          <div class="col">
            <label for="deskripsi" class="text-secondary">Deskripsi Pesanan</label>
            <p class="text-success font-weight-bold text-capitalize">{{ $orderan->deskripsi }}</p>
          </div>
        </div>
        @if ($orderan->status == "Waiting")
        <div class="row mx-0">
          <form action="{{ route('murid.createOfflineAccepted', $orderan->id) }}" method="post">
            @csrf @method('PUT')
            <input type="hidden" name="idOrder" value="{{ $orderan->id }}">
            <button type="submit" class="btn btn-success">Terima Tawaran</button>
          </form>
        </div>
        @elseif($orderan->status === "Process")
        <form action="{{ route('murid.createOfflineFinished') }}" method="post">
          @csrf
          <input type="hidden" class="form-control" value="{{ $orderan->id }}" name="idOrder">
          <button type="submit" class="btn btn-danger">Sudahi Belajar</button>
        </form>
        @endif
      </div>
    </div>
  @endsection
  @section('footer_all')
    @include('partials.footer')
  @endsection
