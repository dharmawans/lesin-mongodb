@extends('layouts.master_guru')
@section('title', 'Hasil Pencarian')
@section('content')
<div class="container py-5" id="hasil_cari">
  <div class="row position-relative justify-content-between m-0 height-full">
    <aside class="col-12 col-md-3 card shadow-sm px-1 py-3">
      <form action="" class="p-3">
        <div class="mb-4">
          <label for="jenjang">Jenjang Sekolah</label>
          <select name="" id="jenjang" class="custom-select">
            <option value="" selected>Semua Jenjang</option>
            <option value="">SD</option>
            <option value="">SMP</option>
            <option value="">SMK</option>
          </select>
        </div>
        <div class="mb-4">
          <label for="matpel">Mata Pelajaran</label>
          <select name="" id="matpel" class="custom-select">
            <option value="" selected>Semua Pelajaran</option>
            <option value="Bahasa Indonesia">Bahasa Indonesia</option>
            <option value="Matematika">Matematika</option>
            <option value="Bahasa Inggris">Bahasa Inggris</option>
            <option value="IPA">IPA</option>
            <option value="IPS">IPS</option>
          </select>
        </div>
        <div class="mb-4">
          <label for="list_harga">Cari Berdasarkan :</label>
          <select name="" id="list_harga" class="custom-select">
            <option value="" selected>Acak</option>
            <option value="">Termahal</option>
            <option value="">Termurah</option>
            <option value="">Bintang Terbaik</option>
          </select>
        </div>
        <button type="submit" name="button" class="btn btn-success w-100">Cari Murid</button>
      </form>
    </aside>
    <section class="col-12 col-md-8 float-right">
      <div class="row flex-column">
        @isset($orders)
          @foreach($orders as $order)
          <article class="col-12 mb-3 card shadow-sm p-0">
            <figure class="m-0 p-3 height-full">
                  <img src="{{ asset('photoguru/'.$order->photo_murid) }}" height="50" class="float-left mr-3">
                  <figcaption class="position-relative">
                    <strong class="d-block">{{ $order->studentname }}</strong>
                    <em class="d-block text-secondary my-1">Nama Pelajaran <strong>{{ $order->pelajaran }}</strong></em>
                    <em class="d-block text-secondary my-1 materi_pelajaran">Materi Pelajaran <strong>{{ $order->kategori_pelajaran }}</strong></em>
                    <span class="text-info text-capitalize">
                      harga : <var class="font-weight-bold" style="color: #DE891A;">{{ $order->lesco }} Lesco</var>
                    </span>
                  </figcaption>
            </figure>
            <div class="py-2 text-center border-top" style="background-color: #D0D0D0;">
              <a href="{{route('guru.searchOfflineDetail', $order->id) }}">Lihat Lebih Lanjut</a>
            </div>
          </article>
          @endforeach
        @endisset
      </div>
    </section>
  </div>
</div>
@endsection
@section('footer_all')
  @include('partials.footer')
@endsection
