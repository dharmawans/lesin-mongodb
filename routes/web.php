<?php

Auth::routes();
Route::view('/', 'homepage');
Route::get('home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'MuridRole'])->group(function(){
	Route::prefix('murid')->group(function() {
		Route::name('murid.')->group(function(){
			Route::get('room', 'VideoRoomController@indexMurid')->name('room.index');
			Route::post('room/join', 'VideoRoomController@joinRoomMurid')->name('room.join');
			Route::get('dashboard','ProfileController@indexMurid')->name('index');
			Route::get('detail/{id}','OrderController@detailOrderMurid')->name('detail');
			Route::get('detail/delete/{id}', 'OrderController@hapusOrderan')->name('hapusOrderan');
			Route::get('les/offline/order','OrderController@createOffline')->name('createOffline');
			Route::post('les/offline/order','OrderController@createOfflinePost')->name('createOfflinePost');
			Route::put('les/offline/accept/{id}', 'OrderController@createOfflineAccepted')->name('createOfflineAccepted');
			Route::post('les/offline/finish','OrderController@createOfflineFinished')->name('createOfflineFinished');
			Route::get('les/online/order','OrderController@createOnline')->name('createOnline');
			Route::post('les/online/order','OrderController@createOnlinePost')->name('createOnlinePost');
			Route::get('les/materi/search','MateriController@searchMateri')->name('searchMateri');
			Route::post('les/materi/search','MateriController@searchMateriPost')->name('searchMateriPost');
			Route::get('materi/{id}','MateriController@materi')->name('materi');
			Route::get('materi/{id}/sertifikat','MateriController@printSertifikat')->name('printSertifikat');
			Route::get('task','OrderController@taskMurid')->name('taskMurid');
			Route::get('settings','HomeController@settingsMurid')->name('settingsMurid');
			Route::post('deposit','ProfileController@depositMuridAction')->name('depositMuridAction');
			Route::get('profile','ProfileController@profileMurid')->name('profileMurid');
			Route::get('isiprofile','ProfileController@isiProfileMurid')->name('isiProfileMurid');
			Route::post('isiprofile','ProfileController@isiProfileMuridAction')->name('isiProfileMuridAction');
			Route::get('updateprofile/{id}','ProfileController@updateProfileMurid')->name('updateProfileMurid');
			Route::post('updateprofile','ProfileController@updateProfileMuridAction')->name('updateProfileMuridAction');
			Route::post('buyvideo','MyVideosController@buyVideo')->name('buyVideo');
			Route::get('watch/{title}', 'MateriController@watchmateri')->name('watch');
		});
	});
});
Route::middleware(['auth', 'GuruRole'])->group(function(){
	Route::prefix('guru')->group(function(){
		Route::name('guru.')->group(function(){
			Route::get('room', 'VideoRoomController@indexGuru')->name('room.index');
			Route::get('room/join/{roomName}', 'VideoRoomController@joinRoom')->name('group.room.join');
			Route::post('room/create', 'VideoRoomController@createRoom')->name('room.create');
			Route::get('dashboard','ProfileController@indexGuru')->name('index');
			Route::get('les/offline/order','MateriController@searchOffline')->name('searchOffline');
			Route::get('les/offline/order/{id}','MateriController@searchOfflineDetail')->name('searchOfflineDetail');
			Route::post('les/offline/order','MateriController@searchOfflineAction')->name('searchOfflineTake');
			Route::get('les/offline/filter','MateriController@searchOfflineFilter')->name('searchOfflineFilter');
			Route::post('les/offline/proses','MateriController@searchOfflineProcess')->name('searchOfflineProses');
			Route::post('les/offline/finish','MateriController@searchOfflineFinished')->name('searchOfflineFinished');
			Route::get('les/online/order','MateriController@searchOnline')->name('searchOnline');
			Route::get('les/online/order/{id}','MateriController@searchOnlineDetail')->name('searchOnlineDetail');
			Route::get('les/materi/create','MateriController@createMateri')->name('createMateri');
			Route::post('les/materi/create','MateriController@createMateriPost')->name('createMateriPost');
			Route::get('task','OrderController@taskGuru')->name('taskGuru');
			Route::get('settings','HomeController@settingsGuru')->name('settingsGuru');
			Route::get('withdraw','ProfileController@withdrawGuru')->name('withdrawGuru');
			Route::post('withdraw','ProfileController@withdrawGuruAction')->name('withdrawGuruAction');
			Route::get('profile','ProfileController@profileGuru')->name('profileGuru');
			Route::get('isiprofile','ProfileController@isiProfileGuru')->name('isiProfileGuru');
			Route::post('isiprofile','ProfileController@isiProfileGuruAction')->name('isiProfileGuruAction');
			Route::get('updateprofile/{id}','ProfileController@updateProfileGuru')->name('updateProfileGuru');
			Route::post('updateprofile','ProfileController@updateProfileGuruAction')->name('updateProfileGuruAction');
		});
	});
});
