<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MyVideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('my_videos')->insert([
          'guru' => 'Muhammad Fikri Hadian',
          'iduser' => 1,
          'judul' => 'Belajar Aljabar',
          'deskripsi' => 'Belajar Aljabar dengan teknik yang mudah dimengerti',
          'video' => 'https://www.youtube.com/watch?v=xbe_Logx4LE',
          'sampul' => 'aljabar.jpeg',
          'lesco' => '10',
      ]);
    }
}
