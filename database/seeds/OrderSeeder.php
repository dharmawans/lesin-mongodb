<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('orders')->insert([
        [
          'iduser' => 1,
          'durasi' => 2,
          'lesco' => 200,
          'kategori_pelajaran' => 'Aljabar',
          'pelajaran' => 'Matematika',
          'deskripsi' => 'Saya kesulitan belajar aljabar :(',
          'alamat_ketemu' => 'Jalan J No 17/G, Kebon Baru, Tebet, Jakarta Selatan',
          'status' => 'Waiting',
          'jenjang' => 'SMA',
          'type' => 'Offline',
          'created_at' => date('Y-m-d H:i:s'),
        ],
        [
          'iduser' => 1,
          'durasi' => 1,
          'lesco' => 100,
          'kategori_pelajaran' => 'Advertising',
          'pelajaran' => 'Bahasa Inggris',
          'deskripsi' => 'Saya kesulitan belajar advertising :(',
          'jenjang' => 'SMK',
          'alamat_ketemu' => 'KFC Margonda',
          'status' => 'Waiting',
          'type' => 'Online',
          'created_at' => date('Y-m-d H:i:s'),
        ]
      ]);
    }
}
