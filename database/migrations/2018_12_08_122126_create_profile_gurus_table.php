<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_gurus', function (Blueprint $table) {
          $table->increments('id');
          $table->string('photo');
          $table->unsignedInteger('iduser');
          $table->unsignedInteger('lesco')->nullable();
          $table->string('ktp');
          $table->string('norek');
          $table->string('phone')->default('photoguru/people.png');
          $table->text('alamat');
          $table->text('tentang');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_gurus');
    }
}
