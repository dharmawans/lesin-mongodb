<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('iduser');
          $table->string('kategori_pelajaran');
          $table->string('pelajaran');
          $table->string('teachername')->nullable();
          $table->text('deskripsi');
          $table->integer('durasi');
          $table->text('alamat_ketemu');
          $table->integer('lesco');
          $table->enum('jenjang', ['SMA', 'SMK', 'SD', 'SMP', 'Kuliah']);
          $table->enum('status', ['Waiting', 'Accept', 'Process', 'Success'])->default('Waiting');
          $table->enum('type', ['Offline','Online']);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
