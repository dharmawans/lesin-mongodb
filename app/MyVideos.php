<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyVideos extends Model
{
    protected $fillable = [
      'iduser', 'guru', 'judul', 'deskripsi', 'video', 'lesco', 'sampul', 'created_at'
    ];
}
