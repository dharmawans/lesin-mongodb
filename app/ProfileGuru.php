<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileGuru extends Model
{
    protected $fillable = [
      'iduser', 'photo', 'lesco', 'ktp', 'norek', 'phone', 'alamat', 'tentang'
    ];
    public function myuser(){
    	return $this->hasOne('App\User', 'iduser', 'id');
    }
}
