<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileMurid extends Model
{
    protected $fillable = [
      'idmurid', 'photo', 'lesco', 'sekolah', 'jenjang', 'nisn', 'norek', 'phone', 'alamat', 'tentang'
    ];
    public function profile(){
    	return $this->belongsTo('App\User','idmurid','id');
    }
}
