<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesco extends Model
{
    public function phoneOwner()
    {
      return $this->belongsTo('App\User', 'iduser', 'id');
    }
}
