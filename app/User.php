<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'jenjang', 'role', 'photo', 'phone', 'alamat'
    ];

    public function userLesco()
    {
      return $this->hasOne('App\Lesco', 'iduser', 'id');
    }

    public function profilGuru()
    {
      return $this->belongsTo('App\ProfileGuru', 'iduser', 'id');
    }
    public function profileMurid()
    {
      return $this->hasOne('App\ProfileMurid', 'iduser', 'id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
