<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'iduser', 'teachername', 'pelajaran', 'kategori_pelajaran', 'deskripsi', 'durasi', 'lesco', 'alamat_ketemu', 'status',
        'jenjang', 'type'
    ];

    public function orderOwner()
    {
      return $this->belongsTo('App\User', 'iduser', 'id');
    }

    public function profileOwner()
    {
      return $this->belongsTo('App\ProfileMurid', 'idmurid', 'id');
    }
}
