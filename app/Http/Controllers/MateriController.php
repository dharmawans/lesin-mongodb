<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
Use App\User;
Use App\ProfileGuru;
Use App\ProfileMurid;
Use App\Order;
Use App\Videos;
Use App\MyVideos;
use Auth;
use PDF;

class MateriController extends Controller
{
  public function createMateri(){
    $profil = ProfileGuru::where('iduser', Auth::id())->first();
    return view('guru.createMateri', compact('profil'));
  }
  public function createMateriPost(Request $r){
        $filephoto = $r->file('sampul');
        $gambar = $filephoto->getClientOriginalName();
        $r->file('sampul')->move(public_path('sampul'), $gambar);

        $filevideo = $r->file('videos');
        $videonya = $filevideo->getClientOriginalName();
        $r->file('videos')->move(public_path('uploaded_video'), $videonya);

        $data = new Videos;
        $data->guru = Auth::user()->name;
        $data->judul = $r->judul;
        $data->deskripsi = $r->deskripsi;
        $data->video = $videonya;
        $data->sampul = $gambar;
        $data->jenjang = $r->jenjang;
        $data->lesco = $r->lesco;
        $data->save();
        return redirect()->route('guru.index');
  }
  public function watchmateri($title){
    $data['myvideos'] = MyVideos::find($title);
    return view('murid.watch', $data);
  }

  public function materi($id) {
      $data['videos'] = Videos::where('id', $id)->get();
      return view('murid.materi', $data);
  }
  public function searchMateri(){
      $profil = ProfileMurid::where('iduser', Auth::id())->first();
      $videonya = Videos::where('jenjang', Auth::user()->jenjang)->get();
      return view('murid.cariMateri', compact('profil', 'videonya'));
  }
  public function searchMateriPost(Request $r){
      $data['videos'] = Videos::where('judul', 'LIKE', '%' . $r->materi . '%')->get();
      return view('murid.resultMateri', $data);
  }
  public function searchOffline(){
    $data['profil'] = ProfileGuru::where('iduser', Auth::id())->get();
    $data['orders'] = Order::where([
                        ['status', 'Waiting'],
                        ['type', 'Offline'],
                      ])->orderBy('id', 'desc')->get();
    return view('guru.searchOffline', $data);
  }
  public function searchOfflineFilter(Request $r){
    $data['profil'] = ProfileGuru::where('iduser', Auth::id())->get();
    $data['orders'] = Order::where([
                        ['status', 'Waiting'],
                        ['type', 'Offline'],
                      ])->orderBy('id', 'desc')->get();
    $data['filter'] = Order::where([
                        ['jenjang', $r->jenjang],
                        ['kategori_pelajaran', '%' . $r->materi . '%'],
                        ['pelajaran', $r->pelajaran]
                      ])
                      ->orWhere('pelajaran', $r->pelajaran)
                      ->orWhere('kategori_pelajaran', '%' . $r->materi . '%')
                      ->orWhere('jenjang', $r->jenjang)->orderBy('id', 'desc')->get();
    return view('guru.hasil_filter', $data);
  }
  public function searchOfflineDetail($id){
      $order = Order::findOrFail($id);
      return view('guru.searchOfflineDetail', ['orderan' => $order]);
  }
  public function searchOfflineAction(Request $r){
    $update = Order::find($r->idOrder);
    $update->teachername = $r->teacherName;
    $update->status = "Accept";
    $update->save();

    return redirect()->route('guru.taskGuru');
  }
  public function searchOfflineProcess(Request $r){
    $process = Order::find($r->idOrder);
    $process->status = "Process";
    $process->save();

    return redirect()->route('guru.taskGuru');
  }
  public function searchOfflineFinished(Request $r){
      $status = Order::find($r->idOrder);
      $status->status = "Success";
      $status->save();

      $profile = ProfileGuru::where('iduser', Auth::id())->first();
      $profile->lesco += $r->lesco;
      $profile->save();
      return redirect()->route('guru.taskGuru');
  }
  public function searchOnline()
  {
      $profil = ProfileGuru::where('iduser', Auth::id())->get();
      $orders = Order::where([
                  ['status', 'waiting'],
                  ['type', 'Online'],
                ])->orderBy('id', 'desc')->get();
      return view('guru.hasil_cari', compact('profil', 'orders'));
  }
  public function printSertifikat($id)
  {
      $data['users'] = User::where('name', Auth::user()->name)->get();
      $data['videos'] = MyVideos::where('id', $id)->get();
      $pdf = PDF::loadview('murid.sertifikat-materi', $data);
      // return $pdf->download('sertifikat-materi.pdf');
      return $pdf->stream();
  }
}
