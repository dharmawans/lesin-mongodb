<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
Use App\User;
Use App\ProfileGuru;
Use App\ProfileMurid;
Use App\Order;
Use App\Videos;
Use App\MyVideos;
use Auth;
use PDF;

class OrderController extends Controller
{
  public function taskMurid(){
      $data['profil'] = ProfileMurid::where('iduser', Auth::id())->get();
      $data['profile_murid'] = ProfileMurid::where('iduser', Auth::id())->get();
      $data['orders'] = Order::where([
                          ['iduser', Auth::id()],
                          ['status', '!=', 'Success'],
                        ])->orderBy('id', 'desc')->paginate(5);
      return view('murid.task', $data);
  }
  public function taskGuru(){
    $data['profil'] = ProfileGuru::where('iduser', Auth::id())->get();
      $data['orders'] = Order::where([
                          ['iduser', Auth::id()],
                          ['status','!=','Success']
                        ])->orderBy('id', 'desc')->get();
      return view('guru.task', $data);
  }
  public function createOffline(){
    $profil = ProfileMurid::where('iduser', Auth::id())->first();
    return view('murid.createOffline', ['profil' => $profil]);
  }
  public function createOfflinePost(Request $r){
     $post = new Order;
     $post->pelajaran = $r->pelajaran;
     $post->kategori_pelajaran = $r->kategori_pelajaran;
     $post->deskripsi = $r->deskripsi;
     $post->durasi = $r->durasi;
     $post->lesco = $r->lesco;
     $post->alamat = $r->alamat;
     $post->jenjang = $r->jenjang;
     $post->type = "Offline";
     $post->save();

     $profile = ProfileMurid::where('iduser', Auth::id())->first();
     $profile->lesco -= $r->lesco;
     $profile->save();
     return redirect()->route('murid.taskMurid');
  }
  public function createOfflineFinished(Request $r){
      $upd_order = Order::where('id', $r->idOrder);
      $upd_order->status = "Success";
      $upd_order->save();

      return redirect()->route('murid.taskMurid');
  }
  public function createOfflineAccepted(Request $request, $id) {
    $acc_orderan = Order::where('id', $request->idOrder)->update(['status' => 'Accept']);
    return redirect()->route('guru.taskGuru');
  }
  public function createOnline(){
    $profile_murid = ProfileMurid::where('iduser', Auth::id())->get();
    return view('murid.createOnline', compact('profile_murid'));
  }
  public function createOnlinePost(Request $r){
    $post = new Order;
    $post->idmurid = Auth::id();
    $post->studentname = Auth::user()->name;
    $post->teachername = "";
    $post->photo_murid = $r->photo_murid;
    $post->pelajaran = $r->pelajaran;
    $post->deskripsi = $r->deskripsi;
    $post->durasi = $r->durasi;
    $post->lesco = $r->lesco;
    $post->alamat = "";
    $post->status = "Waiting";
    $post->jenjang = $r->jenjang;
    $post->type = "Online";
    $post->save();

    $profile = ProfileMurid::where('iduser', Auth::id())->first();
    $profile->lesco -= $r->lesco;
    $profile->save();
    return redirect()->route('murid.taskMurid');
  }
  public function detailOrderMurid($id){
    $data['profil'] = Order::where('iduser', Auth::id())->get();
    $data['orders'] = Order::findOrFail($id);
    return view('murid.detailOrder', $data);
  }
  public function hapusOrderan($id)
  {
    $flight = Order::findOrFail($id);
    $flight->delete();
    return redirect()->route('murid.taskMurid');
  }
}
