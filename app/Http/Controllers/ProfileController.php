<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
Use App\User;
Use App\ProfileGuru;
Use App\ProfileMurid;
Use App\Order;
Use App\Videos;
Use App\MyVideos;
use Auth;
use PDF;

class ProfileController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function indexMurid()
  {
      $data['profil'] = ProfileMurid::where('iduser', Auth::id())->first();
      return view('murid.dashboard', $data);
  }
  public function indexGuru(){
      $data['profil'] = ProfileGuru::where('iduser', Auth::id())->get();
      return view('guru.index', $data);
  }
  public function profileGuru(){
      $data['videos'] = Videos::where('guru', Auth::user()->name)->paginate(5);
      $data['history'] = Order::where([
                          ['teachername', Auth::user()->name],
                          ['status', 'Success'],
                        ])->paginate(5);
      $data['profil'] = ProfileGuru::where('iduser', Auth::id())->first();
      return view('guru.profile', $data);
  }

  public function isiProfileGuru(){
      $profil = ProfileGuru::where('iduser', Auth::id())->get();
      return view('guru.isi_profile', compact('profil'));
  }
  public function isiProfileGuruAction(Request $r){
      $file = $r->file('photo');
      $gambar = $file->getClientOriginalName();
      $r->file('photo')->move(public_path('photoguru'), $gambar);

      $post = new ProfileGuru;
      $post->iduser = Auth::id();
      $post->photo = $gambar;
      $post->lesco = null;
      $post->ktp = $r->ktp;
      $post->norek = $r->norek;
      $post->phone = $r->phone;
      $post->alamat = $r->alamat;
      $post->tentang = $r->tentang;
      $post->save();
      return redirect()->route('guru.profileGuru');
  }
  public function updateProfileGuru($id){
      $data['profil'] = ProfileGuru::where('id', $id)->get();
      return view('guru.update_profile', $data);
  }
  public function updateProfileGuruAction(Request $r){
      if(isset($r->photo)){
          $file = $r->file('photo');
          $gambar = $file->getClientOriginalName();
          $r->file('photo')->move(public_path('photoguru'), $gambar);

          $update = ProfileGuru::where('iduser', $r->idguru)->first();
          $update->photo = $gambar;
          $update->ktp = $r->ktp;
          $update->norek = $r->norek;
          $update->phone = $r->phone;
          $update->alamat = $r->alamat;
          $update->tentang = $r->tentang;
          $update->save();
      }
      else {
          $update = ProfileGuru::where('iduser', $r->idguru)->first();
          $update->ktp = $r->ktp;
          $update->norek = $r->norek;
          $update->phone = $r->phone;
          $update->alamat = $r->alamat;
          $update->tentang = $r->tentang;
          $update->save();
      }
      return redirect()->route('guru.profileGuru');
  }

  public function profileMurid(){
      $data['profiles'] = User::where('name', Auth::user()->name)->get();
      $data['history'] = Order::where([
                          ['studentname', Auth::user()->name],
                          ['status', 'Success'],
                        ])->get();
      $data['profil'] = ProfileMurid::where('iduser', Auth::id())->first();
      $data['videos'] = MyVideos::where('iduser', Auth::id())->get();
      return view('murid.profile', $data);
  }

  public function isiProfileMurid(){
      $profil = ProfileMurid::where('iduser', Auth::id())->get();
      return view('murid.isi_profile', compact('profil'));
  }
  public function isiProfileMuridAction(Request $r){
      $file = $r->file('photo');
      $gambar = $file->getClientOriginalName();
      $r->file('photo')->move(public_path('photoguru'), $gambar);

      $post = new ProfileMurid;
      $post->idmurid = Auth::id();
      $post->photo = $gambar;
      $post->lesco = null;
      $post->sekolah = $r->sekolah;
      $post->jenjang = $r->jenjang;
      $post->nisn = $r->nisn;
      $post->norek = $r->norek;
      $post->phone = $r->phone;
      $post->alamat = $r->alamat;
      $post->tentang = $r->tentang;
      $post->save();

      $users = User::find(Auth::id());
      $users->jenjang = $r->jenjang;
      $users->save();
      return redirect()->route('murid.profileMurid');
  }
  public function updateProfileMurid($id){
      $data['profil'] = ProfileMurid::where('iduser', Auth::id())->get();
      return view('murid.update_profile', $data);
  }
  public function updateProfileMuridAction(Request $r){
      if (isset($r->photo)) {
        $file = $r->file('photo');
        $gambar = $file->getClientOriginalName();
        $r->file('photo')->move(public_path('photomurid'), $gambar);

        $update = ProfileMurid::where('iduser', Auth::id())->first();
        $update->photo = $gambar;
        $update->sekolah = $r->sekolah;
        $update->jenjang = $r->jenjang;
        $update->nisn = $r->nisn;
        $update->norek = $r->norek;
        $update->phone = $r->phone;
        $update->alamat = $r->alamat;
        $update->tentang = $r->tentang;
        $update->save();

        $users = User::find(Auth::id());
        $users->jenjang = $r->jenjang;
        $users->save();
      }
      else {
        $update = ProfileMurid::where('iduser', Auth::id())->first();
        $update->sekolah = $r->sekolah;
        $update->jenjang = $r->jenjang;
        $update->nisn = $r->nisn;
        $update->norek = $r->norek;
        $update->phone = $r->phone;
        $update->alamat = $r->alamat;
        $update->tentang = $r->tentang;
        $update->save();

        $users = User::find(Auth::id());
        $users->jenjang = $r->jenjang;
        $users->save();
      }
      return redirect()->route('murid.profileMurid');
  }
  public function depositMuridAction(Request $r)
  {
      $profile = ProfileMurid::where('iduser', Auth::id())->first();
      $profile->lesco += $r->lesco;
      $profile->save();
      return redirect()->route('murid.index');
  }
  public function withdrawGuruAction(Request $r)
  {
      $profile = ProfileGuru::where('iduser', Auth::id())->first();
      $profile->lesco -= $r->lesco;
      $profile->save();
      return redirect()->route('guru.index');
  }
}
